import { mkdirSync, writeFileSync } from "fs";
import fetch from "node-fetch";
import path from "path";
import { Type_MCS } from "../types/Monstercat";
import { cleanString } from "./Misc";
import readline = require("readline");

// Process the url
export async function Monstercat_Download(url: string, saveDir: string): Promise<void> {
    // Split the url to get the song id.
    const split = url.split('/'), id = split[split.length - 1];

    // Fetch data from the Monstercat API
    const res: Type_MCS = await (await fetch(`https://connect.monstercat.com/v2/catalog/release/${id}`)).json();

    // Catch errors in json body
    if (res.error || !res.release) {
        console.warn(`Failed to find song with id: ${id}`);
        return;
    }

    // Check if album is in early access
    if (res.release.inEarlyAccess) {
        console.log(`\nUnable to download ${res.release.title}, as it's in early access.`);
        return;
    }

    console.log(`\nNow downloading ${res.release.title} by ${res.release.artistsTitle}`);

    // Clean album name
    const titleClean = cleanString(res.release.title);

    // Album path
    const albumPath = path.join(saveDir, titleClean);

    // Make download path
    mkdirSync(albumPath, { recursive: true });

    // Download tracks
    for (let i = 0; i < res.tracks.length; i++) {
        const track = res.tracks[i];

        // Write progress
        readline.cursorTo(process.stdout, 0);
        process.stdout.write(`Downloading track ${i + 1} of ${res.tracks.length}.`);
        readline.moveCursor(process.stdout, 0, 0);

        // Set track version, usually remixes
        let version = '';
        if (track.version) version = ` (${track.version})`;

        const name = cleanString(`${track.artistsTitle} - ${track.title}${version}.mp3`);

        const url = `https://connect.monstercat.com/v2/release/${res.release.id}/track-stream/${track.id}`;
        const data = await (await fetch(url)).buffer();

        writeFileSync(path.join(albumPath, name), data);
    }

    return;
}
