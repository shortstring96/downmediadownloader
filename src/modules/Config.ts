import { readFileSync, writeFileSync } from "fs";
import { configPath } from "../DownMediaDownloader";
import { configType } from "../types/Config";

// Default config
const config: configType = {
    "M3U8": {
        "downloadThreads": 10
    },
    "Twitch": {
        "downloadComments": true,
        "useFolderRoot": false
    },
    "YouTube": {
        "downloadThreads": 10
    }
};

export function readConfig(): configType {
    let conf: configType;
    try {
        const file = readFileSync(configPath, 'utf8');
        conf = JSON.parse(file);
    } catch (error) {
        console.warn('Failed to read config file.\nWriting defaults.');
        try {
            writeFileSync(configPath, JSON.stringify(config, undefined, 4));
        } catch (error) {
            console.error(`Failed to write default config: ${error}`);
        }
        return config;
    }
    return conf;
}
