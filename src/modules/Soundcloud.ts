import { createWriteStream, mkdirSync } from "fs";
import miniget from "miniget";
import fetch from "node-fetch";
import path from "path";
import { configType } from "../types/Config";
import { Type_SoundcloudStreams } from "../types/Soundcloud";
import { M3U8_Download } from "./M3U8";
import { cleanString } from "./Misc";

let outputDir = __dirname;

export async function Soundcloud_Download(url: string, saveDir: string, m3u8Options?: configType["M3U8"]): Promise<void> {
    outputDir = saveDir;

    // Download site html
    const playHtml = await (await fetch(url)).text();

    // Get array of crossorigin scripts
    const tokenScripts = playHtml.split('<script crossorigin src="');

    let clientId: undefined | string;
    for (const script of tokenScripts) {
        // Close script tag
        const uri = script.split('">')[0];

        // Script uri will start with 48
        if (uri.split('/').reverse()[0].startsWith('48')) {
            const data = await (await fetch(uri)).text();
            // Grab data from first client_id tag
            clientId = data.split('client_id:"')[1].split('",')[0];
            break;
        }
    }

    if (!clientId) throw "Failed to find Soundcloud Client ID";

    // Get track transcodes from html
    const htmlStreams = playHtml.split('"transcodings":[').splice(1);

    // Get track titles from html
    const htmlTitles = playHtml.split('"title":"').splice(1);

    // Parse list of tracks
    const tracks: string[] = [];
    for (let i = 0; i < htmlStreams.length; i++) {
        const track = htmlStreams[i];
        tracks.push(track.split(']', 1)[0]);
    }

    // Parse list of titles
    const titles: string[] = [];
    for (let i = 0; i < htmlTitles.length; i++) {
        const title = htmlTitles[i];
        titles.push(title.split('"', 1)[0]);
    }

    console.log(`Downloading "${titles[0]}" from Soundcloud`);

    // Loop for tracks
    for (let i = 0; i < tracks.length; i++) {
        const track = tracks[i];

        // Get track title, default to album title if none
        const title = titles[i + 1] ?? titles[0];

        // Parse track data to json
        let streams: Type_SoundcloudStreams;
        try {
            streams = JSON.parse(`[${track}]`);
        } catch (error) {
            console.log('Failed to download Soundcloud link');
            continue;
        }

        let hlsMp3: undefined | string, progressiveMp3: undefined | string;

        // Check available formats for track
        for (let x = 0; x < streams.length; x++) {
            const stream = streams[x];

            // Ignore streams that aren't mpeg audio
            if (stream.format.mime_type !== "audio/mpeg") continue;

            // Get the playlist
            const playlist: { "url"?: string } = await (await fetch(`${stream.url}?client_id=${clientId}`)).json();

            // Ignore playlists missing url
            if (!playlist.url) continue;

            // Sort protocols
            if (stream.format.protocol === "hls") hlsMp3 = playlist.url;
            if (stream.format.protocol === "progressive") progressiveMp3 = playlist.url;
        }

        // Set save dir
        const saveDir = path.join(outputDir, cleanString(titles[0]));

        // Progressive save path
        const save = path.join(saveDir, cleanString(`${title}.mp3`));

        // Make save directory
        mkdirSync(saveDir, { recursive: true });

        // Better to use a progressive stream if we can
        if (progressiveMp3) {
            await downloadTrack(progressiveMp3, save);
            continue;
        }

        // Fallback to hls
        if (hlsMp3) {
            await M3U8_Download(hlsMp3, saveDir, m3u8Options, title);
            continue;
        }

        console.log(`No compatible streams found.`);
    }
    return;
}

function downloadTrack(url: string, path: string): Promise<void> {
    const stream = miniget(url);
    const ws = createWriteStream(path);
    return new Promise((resolve) => {
        stream.pipe(ws).once('finish', resolve);
    });
}
