import { createWriteStream } from "fs";
import miniget from "miniget";

export type DownloadArray = { URL: string, path: string, requestOptions?: miniget.Options }[];

export default async function arraydl(array: DownloadArray, options?: { maxRetries?: number, threads?: number, skip?: number[] }, progress?: (bytesDownloaded: number, percent: number, failed: number, complete: number[]) => void): Promise<void> {
    return new Promise((resolve) => {
        // Set default settings
        if (!options) {
            options = {
                maxRetries: 5,
                threads: 5
            };
        }
        if (!options?.maxRetries) {
            options.maxRetries = 5;
        }
        if (!options.threads) {
            options.threads = 5;
        }

        // Use at least one thread
        if (options.threads <= 0) {
            options.threads = 1;
        }

        // Progress
        const completeSegments: number[] = [];
        let total = array.length;
        let totalDownloaded = 0;
        let segmentIndex = 0;
        let complete = 0;
        let failed = 0;
        let end = false;

        // Update array with resumeIndex
        if (options.skip) {
            for (let i = 0; i < options.skip.length; i++) {
                const skipIndex = options.skip[i];
                completeSegments.push(skipIndex);
                array.splice(skipIndex - i, 1);
            }
            total = array.length;
        }

        // Create threads
        const threads = options?.threads ?? 5;
        for (let i = 0; i < threads; i++) download();

        // Download loop
        async function download(retry?: { item: DownloadArray[0], segmentIndex: number, retryIndex: number }): Promise<void> {
            // Stop trying download after maxRetries
            if (retry && retry.retryIndex > (options?.maxRetries ?? 5)) {
                failed++;
                download();
                return;
            }

            const currentIndex = retry?.segmentIndex ?? segmentIndex++;

            // Get url to download
            const current = retry?.item ?? array.shift();

            // Make sure we have a url
            if (!current) {
                // Check if main array is empty and we haven't already ended
                if (array.length === 0 && !end) {
                    const inv = setInterval(() => {
                        // Check if all items have a status
                        if (complete + failed === total) {
                            resolve();
                            clearInterval(inv);
                        }
                    }, 1000);

                    // Set end
                    end = true;
                }
                return;
            }

            let req: miniget.Stream;
            try {
                req = miniget(current.URL, current.requestOptions);
            } catch (error) {
                const retryIndex = retry?.retryIndex ?? 0;
                download({ item: current, segmentIndex: currentIndex, retryIndex: retryIndex + 1 });
                return;
            }

            // Write file to disk
            const ws = createWriteStream(current.path);
            req.pipe(ws);

            let localBytesDownloaded = 0;
            req.on('data', (chunk) => {
                totalDownloaded += chunk.length;
                localBytesDownloaded += chunk.length;
                if (progress) progress(totalDownloaded, complete / total * 100, failed, completeSegments);
            });

            // Sometimes downloads just seem to stall, catch and restart
            let lastKnownBytes = 0;
            const inv = setInterval(() => {
                if (localBytesDownloaded === lastKnownBytes) {
                    clearInterval(inv);
                    req.destroy();

                    // Try again after 10 seconds
                    setTimeout(() => {
                        const retryIndex = retry?.retryIndex ?? 0;
                        download({ item: current, segmentIndex: currentIndex, retryIndex: retryIndex + 1 });
                    }, 1000 * 10);

                    // Remove from totalDownloaded
                    totalDownloaded -= localBytesDownloaded;
                }
                lastKnownBytes = localBytesDownloaded;
            }, 1000 * 10);

            // Catch errors
            let alreadyErrored = false;
            req.on('error', (err) => {
                clearInterval(inv);
                if (alreadyErrored) return;
                alreadyErrored = true;
                totalDownloaded -= localBytesDownloaded;
                if (err.statusCode === 404) {
                    failed++;
                    download();
                } else {
                    const retryIndex = retry?.retryIndex ?? 0;
                    download({ item: current, segmentIndex: currentIndex, retryIndex: retryIndex + 1 });
                }
                if (progress) progress(totalDownloaded, complete / total * 100, failed, completeSegments);
            });

            ws.on('finish', () => {
                clearInterval(inv);
                completeSegments.push(currentIndex);
                completeSegments.sort((a, b) => a - b);
                complete++;
                if (progress) progress(totalDownloaded, complete / total * 100, failed, completeSegments);
                download();
            });
        }
    });
}
