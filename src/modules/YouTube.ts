import ffmpeg from "fluent-ffmpeg";
import { createReadStream, createWriteStream, mkdirSync, readFileSync, unlinkSync, writeFile } from "fs";
import path from "path";
import readline from "readline";
import { chooseFormat, getInfo } from "ytdl-core";
import { configType } from "../types/Config";
import arraydl, { DownloadArray } from "./ArrayDL";
import { cleanString, fileExists } from "./Misc";

// Download threads
let threads = 10;

let outputDir: string = __dirname;

export async function YouTube_Download(url: string, saveDir: string, options?: configType["YouTube"]): Promise<void> {
    // Update output dir
    outputDir = saveDir;

    // Update options
    if (options) {
        threads = options.downloadThreads;
    }

    // Check if temp dir exists
    if (fileExists(path.join(outputDir, 'temp'))) {
        // These file will cause issues if they exist before a download starts
        try {
            unlinkSync(path.join(outputDir, 'temp', 'video'));
        } catch (error) { /***/ }
        try {
            unlinkSync(path.join(outputDir, 'temp', 'audio'));
        } catch (error) { /***/ }
    } else {
        // Make temp dir
        mkdirSync(path.join(outputDir, 'temp'), { recursive: true });
    }

    // Get video info
    const info = await getInfo(url);

    // Error if info is undefined
    if (!info) {
        return console.warn(`Failed to get info for video ${url}\nTry again later.`);
    }

    // Remove invalid characters from title
    const title = cleanString(info.videoDetails.title);

    // Check if video already exists (with mp4 or webm containers)
    if (fileExists(path.join(outputDir, `${title}.mp4`)) || fileExists(path.join(outputDir, `${title}.webm`))) {
        return console.log(`\nSkipping download of: ${info.videoDetails.title}\nFile already exists.`);
    }

    console.log(`\nNow downloading: ${info.videoDetails.title}`);

    // Clone formats as it will be modified
    const formatsClone = [...info.formats];

    // Remove audio only formats or formats missing content length
    for (let i = 0; i < formatsClone.length; i++) {
        const format = formatsClone[i];
        if (!format.hasVideo || !format.contentLength) {
            formatsClone.splice(i, 1);
            i--;
        }
    }

    // Sort by Highest quality, smallest size.
    formatsClone.sort((a, b) => {
        const widthFramerateA = +`${a.width}${a.fps}`;
        const widthFramerateB = +`${b.width}${b.fps}`;

        if (widthFramerateA < widthFramerateB) {
            return 1;
        } else {
            if (+a.contentLength < +b.contentLength) {
                return -1;
            } else {
                if (widthFramerateA !== widthFramerateB) {
                    return -1;
                }
                return 1;
            }
        }
    });

    // First item will be highest quality and smallest size
    const videoFormat = formatsClone[0];

    if (!videoFormat) {
        console.error('No video formats found.');
        return;
    }

    // Convert content length to number
    const videoSize = parseInt(videoFormat.contentLength);

    // Bytes per stream
    const bytesPerStream = 1000 * 1000 * 5;

    // Calculate number of segments to download
    let totalVideoSegments = Math.round(videoSize / bytesPerStream);

    // We must have at least one video segment
    if (totalVideoSegments === 0) totalVideoSegments = 1;

    // Total download size
    let totalSize = videoSize;

    // Thread start byte
    let nextStartByte = 0;

    // Segment queue
    const segments: { "url": string, "startByte": number, "endByte": number, "isAudio"?: boolean }[] = [];

    // Calculate and push video segments
    for (let i = 0; i < totalVideoSegments; i++) {
        let end = nextStartByte + bytesPerStream;

        // Don't exceed source length
        if (end > videoSize) end = videoSize;

        // Make sure last segment gets all remaining data
        if (i + 1 === totalVideoSegments && videoSize > end) end = videoSize;
        segments.push({ "url": videoFormat.url, "startByte": nextStartByte, "endByte": end });
        nextStartByte = end + 1;
    }

    // Add audio if needed
    if (!videoFormat.hasAudio) {
        const audioFormat = chooseFormat(info.formats, {
            quality: "highestaudio",
            filter: audioFormat => audioFormat.container === videoFormat.container && !audioFormat.hasVideo
        });

        // This is mostly the same as what we do above for video
        const audioLength = parseInt(audioFormat.contentLength);
        totalSize += audioLength;
        let totalAudioSegments = Math.round(audioLength / bytesPerStream);
        if (totalAudioSegments < 1) totalAudioSegments = 1;
        nextStartByte = 0;

        // Do same as video segments
        for (let i = 0; i < totalAudioSegments; i++) {
            let end = nextStartByte + bytesPerStream;
            if (end > audioLength) end = audioLength;
            if (i + 1 === totalAudioSegments && audioLength > end) end = audioLength;
            segments.push({ "url": audioFormat.url, "startByte": nextStartByte, "endByte": end, "isAudio": true });
            nextStartByte = end + 1;
        }
    }

    // Print details about download
    console.log(`Video codec: ${videoFormat.videoCodec}`);
    console.log(`Segments: ${segments.length} (${totalVideoSegments} Video, ${segments.length - totalVideoSegments} Audio)`);
    console.log(`Size: ${totalSize} (${videoFormat.contentLength} Video, ${totalSize - +videoFormat.contentLength} Audio)`);
    console.log(`Resolution: ${videoFormat.width}x${videoFormat.height}@${videoFormat.fps}`);

    // Convert our segment array into a DownloadArray
    const downloadArray: DownloadArray = [];
    for (let i = 0; i < segments.length; i++) {
        const segment = segments[i];
        const segPath = path.join(outputDir, 'temp', `${i}`);
        downloadArray.push({ URL: segment.url, path: segPath, requestOptions: { headers: { "Range": `bytes=${segment.startByte}-${segment.endByte}` } } });
    }

    const startDate = Date.now();

    // Write file with list of complete segments
    let completeSeg: number[], lastWritten: number[];
    const fileInv = setInterval(() => {
        if (completeSeg?.length === lastWritten?.length) return;
        lastWritten = [...completeSeg];
        writeFile(path.join(outputDir, 'temp', 'complete'), completeSeg.toString(), (err) => {
            if (err) console.error(err);
        });
    }, 5 * 1000);

    let resumeData: undefined | number[];

    if (fileExists(path.join(outputDir, 'temp', 'complete'))) {
        console.log("Resuming download");
        const raw = readFileSync(path.join(outputDir, 'temp', 'complete'), 'utf-8');
        const splitRaw = raw.split(',');
        resumeData = [];
        for (const int of splitRaw) {
            resumeData?.push(+int);
        }
    }

    // Download our segments
    await arraydl(downloadArray, { threads: threads, skip: resumeData }, (totalBytesDownloaded, _percent, _failed, complete) => {
        completeSeg = complete;
        const toMB = (bytes: number) => { return (bytes / 1000 / 1000).toFixed(2); };
        const percent = totalBytesDownloaded / totalSize * 100;
        const timeRemaining = (Date.now() - startDate) / 1000 / 60 / (totalBytesDownloaded / totalSize) - (Date.now() - startDate) / 1000 / 60;
        readline.cursorTo(process.stdout, 0);
        process.stdout.write(`Downloading ${percent.toFixed(2)}% (${toMB(totalBytesDownloaded)}MB/${toMB(totalSize)}MB) ~${timeRemaining.toFixed(2)} minutes remaining. `);
        readline.moveCursor(process.stdout, 0, 0);
    });

    // Stop updating progress file
    clearInterval(fileInv);

    // Print newline
    process.stdout.write('\n');

    // Assemble segments
    for (let i = 0; i < segments.length; i++) {
        const segment = segments[i];

        let outName = "video";
        if (segment.isAudio) outName = "audio";

        // Write progress
        readline.cursorTo(process.stdout, 0);
        process.stdout.write(`Writing segments ${((i + 1) / segments.length * 100).toFixed(2)}%`);
        readline.moveCursor(process.stdout, 0, 0);

        const combineSegment = (): Promise<void> => {
            let outpath = path.join(outputDir, 'temp', outName);
            if (videoFormat.hasAudio) {
                outpath = path.join(outputDir, `${title}.mp4`);
            }

            const ws = createWriteStream(outpath, { flags: 'a' });
            const rs = createReadStream(path.join(outputDir, 'temp', i.toString()));
            rs.pipe(ws);

            return new Promise((resolve) => {
                ws.on('close', resolve);
            });
        };

        await combineSegment();

        // Delete segment
        unlinkSync(path.join(outputDir, 'temp', i.toString()));
    }

    // Print newline
    process.stdout.write('\n');

    if (!videoFormat.hasAudio) {
        await finishVideo(path.join(outputDir, 'temp', 'video'), path.join(outputDir, 'temp', 'audio'), path.join(outputDir, `${title}.${videoFormat.container}`));

        // Print newline
        process.stdout.write('\n');

        // Delete temp files
        try {
            unlinkSync(path.join(outputDir, 'temp', 'audio'));
            unlinkSync(path.join(outputDir, 'temp', 'video'));
            unlinkSync(path.join(outputDir, 'temp', 'complete'));
        } catch (error) {
            console.error(error);
            console.warn('Failed to delete some temp files.\nDownload probably failed.');
        }
    }

    return;
}

function finishVideo(vPath: string, aPath: string, outPath: string): Promise<void> {
    return new Promise((resolve) => {
        ffmpeg(vPath)
            .videoCodec('copy') // Set codecs to copy
            .audioCodec('copy')
            .addInput(aPath) // Add audio
            .on('progress', info => {
                if (!info.percent) return; // Sometimes percent is missing
                // This shouldn't be bigger than 100
                if (info.percent > 100) info.percent = 100;
                readline.cursorTo(process.stdout, 0);
                process.stdout.write(`Copying audio ${info.percent.toFixed(2)}%`);
                readline.moveCursor(process.stdout, 0, 0);
            })
            .on('error', err => {
                console.error(err);
                console.error('Audio copy failed');
                resolve();
            })
            .on('end', () => {
                resolve();
            })
            .save(outPath);
    });
}
