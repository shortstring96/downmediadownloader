import { mkdirSync, readFileSync, unlink, writeFileSync } from "fs";
import fetch, { Response } from "node-fetch";
import path from "path";
import readline from "readline";
import { configType } from "../types/Config";
import { Type_TwitchCheermotes, Type_TwitchComments, Type_TwitchInfo, Type_TwitchVideoMoments, t_bttvChannel, t_bttvGlobal, t_ffz } from "../types/Twitch";
import { M3U8_Download } from "./M3U8";
import { cleanString, fileExists } from "./Misc";

const clientId = 'kimne78kx3ncx6brgo4mv6wki5h1ko';
const reqHeaders = { headers: { "Accept": "application/vnd.twitchtv.v5+json", "Client-ID": clientId } };

let outputDir: string = __dirname;

export async function Twitch_download(url: string, saveDir: string, options?: configType["Twitch"], m3u8Options?: configType["M3U8"]): Promise<void> {
    // Update output dir
    outputDir = saveDir;

    // Split url down to video id
    const urlSplit = url.split('?')[0].split('/'),
        id = urlSplit[urlSplit.length - 1];

    // Get vod details
    const info: Type_TwitchInfo = await (await fetch(`https://api.twitch.tv/kraken/videos/${id}`, reqHeaders)).json();

    // Data needed to request an access token
    const tokenData = {
        "operationName": "PlaybackAccessToken_Template",
        "query": "query PlaybackAccessToken_Template($login: String!, $isLive: Boolean!, $vodID: ID!, $isVod: Boolean!, $playerType: String!) {  streamPlaybackAccessToken(channelName: $login, params: {platform: \"web\", playerBackend: \"mediaplayer\", playerType: $playerType}) @include(if: $isLive) {    value    signature    __typename  }  videoPlaybackAccessToken(id: $vodID, params: {platform: \"web\", playerBackend: \"mediaplayer\", playerType: $playerType}) @include(if: $isVod) {    value    signature    __typename  }}",
        "variables": {
            "isLive": false,
            "login": "",
            "isVod": true,
            "vodID": id,
            "playerType": "site"
        }
    };

    // Options for request
    const tokenOptions = { method: 'POST', headers: { "Client-ID": clientId }, body: JSON.stringify(tokenData) };

    // Get video access token
    const videoToken = await (await fetch(`https://gql.twitch.tv/gql`, tokenOptions)).json();

    // Get master playlist
    const playlistURL = `https://usher.ttvnw.net/vod/${id}.m3u8?allow_source=true&sig=${videoToken.data.videoPlaybackAccessToken.signature}&token=${videoToken.data.videoPlaybackAccessToken.value}`;
    const masterPlaylist = await (await fetch(playlistURL)).text();

    // Get playlists
    const playlists: string[] = [];

    for (let i = 0; i < masterPlaylist.split(/\r?\n/).length; i++) {
        const line = masterPlaylist.split(/\r?\n/)[i];
        if (!line.startsWith('#')) playlists.push(line);
    }

    console.log(`\nNow downloading: ${info.title}\nfrom channel: ${info.channel.display_name}`);

    // Warn if the VOD has muted segments
    if (info.muted_segments)
        console.warn('WARNING: This VOD has muted segments!');

    if (!options?.useFolderRoot) {
        outputDir = path.join(outputDir, cleanString(`Twitch - ${info.channel.display_name} - ${info.title} - ${info._id}`));
    }

    // Make output and temp dir
    mkdirSync(path.join(outputDir, "temp"), { recursive: true });

    // Start download
    await M3U8_Download(
        playlists[0],
        outputDir,
        m3u8Options,
        `${info.channel.display_name} - ${info.title}`
    );

    if (options?.downloadComments ?? true) {
        console.log('\nDownloading comments...');
        try {
            await DownloadComments(info);
        } catch (error) {
            console.error(error);
            console.warn('Failed to download comments');
        }
    }

    writeInfo(info);

    return;
}

async function DownloadComments(info: Type_TwitchInfo): Promise<void> {
    let pageToken: string | undefined;
    let comments: Type_TwitchComments["comments"] = [];
    let errors = 0;

    // Check if we have previously saved comments
    const temp = path.join(outputDir, 'temp');
    if (fileExists(path.join(temp, `comments_${info._id}.json`))) {
        try {
            const commentsJson = readFileSync(path.join(temp, `comments_${info._id}.json`), 'utf8');
            comments = JSON.parse(commentsJson);
            console.log('Using cached comments');
        } catch (error) {
            console.warn('Unable to load comments from cache');
        }
    }

    // Loop while we have a pageToken
    while (pageToken || comments.length === 0) {
        let page: Type_TwitchComments;

        try {
            if (!pageToken)
                page = await (await fetch(`https://api.twitch.tv/v5/videos/${info._id.slice(1)}/comments?content_offset_seconds=0`, reqHeaders)).json();
            else
                page = await (await fetch(`https://api.twitch.tv/v5/videos/${info._id.slice(1)}/comments?cursor=${pageToken}`, reqHeaders)).json();
        } catch (error) {
            errors++;
            console.error(`Failed to download comment page (try ${errors})`);
            // Stop after 5 errors
            if (errors >= 5) return;
            continue;
        }

        // Reset error count
        errors = 0;

        pageToken = page._next;

        // Push comments to array
        for (let i = 0; i < page.comments.length; i++)
            comments.push(page.comments[i]);

        // Log progress
        readline.cursorTo(process.stdout, 0);
        process.stdout.write(`Downloaded ${comments.length} messages.`);
        readline.moveCursor(process.stdout, 0, 0);

        if (!page._next) break;
    }

    // Don't do anything if there are no comments
    if (comments.length === 0) return;

    // Save comments to temp file
    writeFileSync(path.join(temp, `comments_${info._id}.json`), JSON.stringify(comments));

    // Newline
    console.log('\n');

    //#region Badges

    const channelSets = await (await fetch(`https://badges.twitch.tv/v1/badges/channels/${info.channel._id}/display`, reqHeaders)).json();
    const globalSets = await (await fetch('https://badges.twitch.tv/v1/badges/global/display', reqHeaders)).json();

    const knownBadges: string[] = [];
    const saveBadges: { "id": string, "url": string }[] = [];

    // Look for badges to save
    for (let i = 0; i < comments.length; i++) {
        const comment = comments[i];
        if (!comment.message.user_badges) continue;

        for (let x = 0; x < comment.message.user_badges.length; x++) {
            const badge = comment.message.user_badges[x];
            if (!badge._id || !badge.version) continue;
            const id = `${badge._id}${badge.version}`;
            if (knownBadges.includes(id)) continue;
            knownBadges.push(id);

            let url: string;

            // Make sure a badge exists with the id and version in channel group
            if (channelSets.badge_sets[badge._id] && channelSets.badge_sets[badge._id].versions[badge.version])
                url = channelSets.badge_sets[badge._id].versions[badge.version].image_url_1x;
            else
                url = globalSets.badge_sets[badge._id].versions[badge.version].image_url_1x;

            saveBadges.push({ "id": id, "url": url });
        }
    }

    const badges: string[] = [];

    // Download badges
    for (let i = 0; i < saveBadges.length; i++) {
        const badge = saveBadges[i];

        readline.cursorTo(process.stdout, 0);
        process.stdout.write(`Downloading badge ${i + 1} of ${saveBadges.length}...`);
        readline.moveCursor(process.stdout, 0, 0);

        const buffer = await (await fetch(badge.url)).buffer();

        badges.push(`"${badge.id}": "${buffer.toString('base64')}"`);
    }

    //#endregion Badges

    // Newline
    console.log('\n');

    //#region Cheer

    const cheerData: Type_TwitchCheermotes = await (await fetch(`https://api.twitch.tv/kraken/bits/actions?channel_id=${info.channel._id}`, reqHeaders)).json();

    const search: Map<string, string> = new Map();

    for (let i = 0; i < cheerData.actions.length; i++) {
        const cheermote = cheerData.actions[i];

        for (let x = 0; x < cheermote.tiers.length; x++) {
            const tier = cheermote.tiers[x];

            if (search.has(`${cheermote.prefix}${tier.id}`)) continue;

            if (cheermote.states.includes("animated")) {
                search.set(`${cheermote.prefix}${tier.id}`, tier.images.dark.animated[1]);
            } else if (cheermote.states.includes("static")) {
                search.set(`${cheermote.prefix}${tier.id}`, tier.images.dark.static[1]);
            }
        }
    }

    // List of cheermotes to save
    const saveCheer: { "key": string, "url": string }[] = [];
    const knownCheer: string[] = [];

    // Look for cheermotes to save
    for (let i = 0; i < comments.length; i++) {
        const comment = comments[i];

        if (!comment.message.bits_spent) continue;

        const words = comment.message.body.split(' ');

        for (let x = 0; x < words.length; x++) {
            const word = words[x];
            const url = search.get(word);

            if (url) {
                if (knownCheer.includes(word)) continue;
                saveCheer.push({ "key": word, "url": url });
                knownCheer.push(word);
            }
        }
    }

    const cheer: string[] = [];

    for (let i = 0; i < saveCheer.length; i++) {
        const savemote = saveCheer[i];

        readline.cursorTo(process.stdout, 0);
        process.stdout.write(`Downloading cheermote ${i + 1} of ${saveCheer.length}...`);
        readline.moveCursor(process.stdout, 0, 0);

        const data = await fetch(savemote.url);
        const buffer = await data.buffer();

        cheer.push(`"${savemote.key}": {"data": "${buffer.toString('base64')}"}`);
    }

    //#endregion Cheer

    // Newline
    console.log('\n');

    //#region Emotes

    const tpEmotes: Map<string, { "id": string, "url": string, "format": string }> = new Map();

    // Download ThirdParty emote manifests
    const bttvGlobal: t_bttvGlobal = await (await fetch('https://api.betterttv.net/3/cached/emotes/global')).json();
    const bttvChannel: t_bttvChannel = await (await fetch(`https://api.betterttv.net/3/cached/users/twitch/${info.channel._id}`)).json();
    const ffzGlobal: t_ffz = await (await fetch('https://api.betterttv.net/3/cached/frankerfacez/emotes/global')).json();
    const ffzChannel: t_ffz = await (await fetch(`https://api.betterttv.net/3/cached/frankerfacez/users/twitch/${info.channel._id}`)).json();

    // Remap global bttv emotes
    for (let i = 0; i < bttvGlobal.length; i++)
        tpEmotes.set(bttvGlobal[i].code, { "id": bttvGlobal[i].id, "url": `https://cdn.betterttv.net/emote/${bttvGlobal[i].id}/`, "format": bttvGlobal[i].imageType });

    // Remap global ffz emotes
    for (let i = 0; i < ffzGlobal.length; i++) {
        const emoteUrl = ffzGlobal[i].images["1x"];
        tpEmotes.set(ffzGlobal[i].code, { "id": ffzGlobal[i].id.toString(), "url": emoteUrl.slice(0, emoteUrl.length - 1), "format": ffzGlobal[i].imageType });
    }

    // Remap channel bttv emotes
    if (bttvChannel.channelEmotes)
        for (let i = 0; i < bttvChannel.channelEmotes.length; i++)
            tpEmotes.set(bttvChannel.channelEmotes[i].code, { "id": bttvChannel.channelEmotes[i].id, "url": `https://cdn.betterttv.net/emote/${bttvChannel.channelEmotes[i].id}/`, "format": bttvChannel.channelEmotes[i].imageType });

    // Remap shared bttv emotes
    if (bttvChannel.sharedEmotes)
        for (let i = 0; i < bttvChannel.sharedEmotes.length; i++)
            tpEmotes.set(bttvChannel.sharedEmotes[i].code, { "id": bttvChannel.sharedEmotes[i].id, "url": `https://cdn.betterttv.net/emote/${bttvChannel.sharedEmotes[i].id}/`, "format": bttvChannel.sharedEmotes[i].imageType });

    // Remap channel ffz emotes
    if (ffzChannel)
        for (let i = 0; i < ffzChannel.length; i++) {
            const emoteUrl = ffzChannel[i].images["1x"];
            tpEmotes.set(ffzChannel[i].code, { "id": ffzChannel[i].id.toString(), "url": emoteUrl.slice(0, emoteUrl.length - 1), "format": ffzChannel[i].imageType });
        }

    // Emotes to save
    const saveEmotes: { "class": "fp" | "tp", "id": string, "url": string, "format"?: string }[] = [];

    // Store IDs that we know so we don't download an emote multiple times
    const knownEmotes: string[] = [];

    // Look for resources to save
    for (let i = 0; i < comments.length; i++) {
        const comment = comments[i];

        // Some messages don't have fragments,
        // we just ignore them right now (They will still be saved)
        if (!comment.message.fragments) {
            console.warn("Skipping comment.");
            continue;
        }

        for (let x = 0; x < comment.message.fragments.length; x++) {
            const frag = comment.message.fragments[x];

            if (frag.emoticon) {
                if (!knownEmotes.includes(frag.emoticon.emoticon_id)) {
                    knownEmotes.push(frag.emoticon.emoticon_id);
                    // Twitch seems to always use light
                    // We add scale during download
                    saveEmotes.push({ "class": "fp", "id": frag.emoticon.emoticon_id, "url": `https://static-cdn.jtvnw.net/emoticons/v2/${frag.emoticon.emoticon_id}/default/light/` });
                }
            } else {
                // Split to words
                const words = frag.text.split(' ');

                // Check if word matches with an emote code
                // I don't know how well this works for matching, but it seems to work
                for (let y = 0; y < words.length; y++) {
                    if (tpEmotes.has(words[y])) {
                        const emote = tpEmotes.get(words[y]);
                        if (emote && !knownEmotes.includes(emote.id)) {
                            knownEmotes.push(emote.id);
                            saveEmotes.push({ "class": "tp", "id": words[y], "url": emote.url, "format": emote.format });
                        }
                    }
                }
            }
        }
    }

    const fp: string[] = [];
    const tp: string[] = [];

    // Download the emotes we found 
    for (let i = 0; i < saveEmotes.length; i++) {
        const emote = saveEmotes[i];

        readline.cursorTo(process.stdout, 0);
        process.stdout.write(`Downloading emote ${i + 1} of ${saveEmotes.length}...`);
        readline.moveCursor(process.stdout, 0, 0);

        let data: Response | undefined;
        let success = false;
        let scale = 5;

        while (!success) {
            if (scale <= 0) continue;
            try {
                let urlScale = `${scale}.0`;
                if (emote.class === "tp" && !emote.url.includes("frankerfacez_emote")) {
                    urlScale = `${scale}x`;
                }
                scale--;
                data = await fetch(emote.url + urlScale);
            } catch (error) {
                console.error(error);
                continue;
            }
            if (data.status === 404) continue;
            success = true;
        }

        if (!data) continue;

        const buffer = await data.buffer();

        if (emote.class === 'fp')
            fp.push(`"${emote.id}": {"data": "${buffer.toString('base64')}"}`);
        else if (emote.class === 'tp')
            tp.push(`"${emote.id}": {"data": "${buffer.toString('base64')}", "format": "${emote.format}"}`);
    }

    //#endregion Emotes

    const output = {
        "version": 1,
        "streamer": {
            "id": info.channel._id,
            "name": info.channel.display_name
        },
        "comments": comments,
        "resources": {
            "badges": JSON.parse(`{${badges}}`),
            "cheermotes": JSON.parse(`{${cheer}}`),
            "emotes": {
                // This is probably bad. I'm sick of fighting with typings, so it will work for now
                "firstParty": JSON.parse(`{${fp}}`),
                "thirdParty": JSON.parse(`{${tp}}`)
            }
        }
    };

    const filePath = path.join(
        outputDir,
        cleanString(`[CHAT] ${info.title}.json`)
    );

    // Write final file
    writeFileSync(filePath, JSON.stringify(output));

    // Remove temp file
    unlink(path.join(temp, `comments_${info._id}.json`), (err) => {
        if (err) console.warn('Failed to remove temp file');
    });

    return;
}

async function writeInfo(info: Type_TwitchInfo): Promise<void> {
    // Get video chapters
    let videoChapters: undefined | Type_TwitchVideoMoments;
    try {
        const reqData = [{
            "operationName": "VideoPreviewCard__VideoMoments",
            "variables": {
                "videoId": info._id.slice(1)
            },
            "extensions": {
                "persistedQuery": {
                    "version": 1,
                    "sha256Hash": "0094e99aab3438c7a220c0b1897d144be01954f8b4765b884d330d0c0893dbde"
                }
            }
        }];

        videoChapters = await (await fetch('https://gql.twitch.tv/gql', { method: 'POST', headers: { "Client-ID": clientId }, body: JSON.stringify(reqData) })).json();
    } catch (error) {
        console.warn('Failed to retrieve some video info');
    }

    const chapters: string[] = [];

    if (videoChapters && videoChapters.length !== 0 && videoChapters[0].data.video.moments.edges.length !== 0) {
        for (let i = 0; i < videoChapters[0].data.video.moments.edges.length; i++) {
            const edge = videoChapters[0].data.video.moments.edges[i].node;
            // TODO: Timestamps and length
            chapters.push(`${edge.details.game.displayName}`);
        }
    }

    const table = `Title: ${info.title}\n`
        + `Channel: ${info.channel.display_name}\n`
        + `Date: ${info.created_at}\n`
        + `Deletion date: ${info.delete_at}\n`
        + `Game: ${info.game}\n`
        + `Chapters:\n${chapters.join(',\n')}`;

    try {
        writeFileSync(path.join(outputDir, 'info.txt'), table);
    } catch (error) {
        console.log('Failed to write info.');
    }
}
