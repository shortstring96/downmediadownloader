import fetch from "node-fetch";
import { configType } from "../types/Config";
import { M3U8_Download } from "./M3U8";

export async function Twitter_Download(url: string, saveDir: string, m3u8Options?: configType["M3U8"]): Promise<void> {
    const videoId = url.split('/')[url.split('/').length - 1];

    const scriptUrl = (await (await fetch(`https://twitter.com/i/videos/tweet/${videoId}`)).text()).split('<script src="')[1].split('"')[0];
    const token = (await (await fetch(scriptUrl)).text()).split('Bearer ')[1].split('"')[0];
    const guest = await (await fetch(`https://api.twitter.com/1.1/guest/activate.json`, { headers: { "Authorization": `Bearer ${token}` } })).json();
    const config = await (await fetch(`https://api.twitter.com/1.1/videos/tweet/config/${videoId}.json`, { headers: { "Authorization": `Bearer ${token}`, "x-guest-token": guest.guest_token } })).json();

    if (!config.track) {
        if (config.errors)
            console.warn(`Download failed with message: ${config.errors[0].message}`);
        else
            console.warn('Download failed');
        return;
    }

    const M3U8 = (await (await fetch(config.track.playbackUrl)).text()).split(/\r?\n/);
    const username = config.track.expandedUrl.split('.com/')[1].split('/')[0];

    await M3U8_Download(`https://video.twimg.com${M3U8[M3U8.length - 2]}`, saveDir, m3u8Options, `Twitter video - ${username} - ${videoId}`);

    return;
}
