export type configType = {
    "M3U8": {
        "downloadThreads": number;
    };
    "Twitch": {
        "downloadComments": boolean;
        "useFolderRoot": boolean;
    };
    "YouTube": {
        "downloadThreads": number;
    };
}
