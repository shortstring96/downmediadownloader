export type Type_SoundcloudStreams = {
    "url": string;
    "preset": string;
    "duration": number;
    "snipped": boolean;
    "format": {
        "protocol": "hls" | "progressive";
        "mime_type": "audio/mpeg" | string;
    }
    "quality": string;
}[]
