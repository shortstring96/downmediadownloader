// Monstercat API response
export type Type_MCS = {
    message: string;
    error: string;
    release: {
        artistsTitle: string;
        catalogId: string;
        downloadable: boolean;
        genreSecondary: string;
        id: string;
        inEarlyAccess: boolean;
        releaseDate: string;
        streamable: boolean;
        title: string;
        type: "Single" | "Album" | "EP";
    };
    tracks: {
        artists: {
            id: string;
            name: string;
            public: boolean;
            role: string;
            uri: string;
        }[];
        artistsTitle: string;
        debutDate: string;
        duration: number;
        explicit: boolean;
        genreSecondary: string;
        id: string;
        inEarlyAccess: boolean;
        playlistSort: number;
        release: {
            artistsTitle: string;
            catalogId: string;
            id: string;
            releaseDate: string;
            title: string;
            type: "Single" | "Album" | "EP"
        },
        streamable: boolean;
        title: string;
        trackNumber: number;
        version: string;
    }[];
};
